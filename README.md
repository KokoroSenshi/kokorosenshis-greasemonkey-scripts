# README #

This contains my Greasemonkey scripts, simply put.

### How do I get set up? ###

TO install a Greasemonkey script, get Greasemonkey [here](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) for Firefox, and I believe Tampermonkey for Chrome, though my scripts are for use with Firefox.

### Contribution guidelines ###

Feedback is nice!