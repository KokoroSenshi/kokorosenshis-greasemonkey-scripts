﻿// ==UserScript==
// @name        Nice Print (Unimelb Timetable)
// @namespace   kokorosenshi
// @description Sorts out your Melbourne University timetable so it is nice to print. Clicking the button will change the elements on the page. DIsclaimer: I do not see how this could affect your timetable set-up, but use at your own risk (i.e. check that your timetable is still fine afterwards)
// @include     https://prod.ss.unimelb.edu.au/student/SM/StudentTtable10.aspx?r=%23UM.STUDENT.APPLICANT&f=%24S1.EST.TIMETBL.WEB*
// @version     1.3.0
// @grant       none
// ==/UserScript==

//RESOLVED? Seems to have disappeared//BUG1: When using Print Preview (firefox) the button appears in the printout even after it was removed. It is fine (i.e. absent) using the normal print tool.
console.log("Nice Print has started");

///// Create, and make useable, a nice button (Referenced: https://web.archive.org/web/20150808040128/https://stackoverflow.com/questions/6480082/add-a-javascript-button-using-greasemonkey-or-tampermonkey)
// Make the button: create the div, button inner HTML, set the div id, add it to the page
var ButtonNode = document.createElement('div');
ButtonNode.innerHTML = '<button id="nicePrintButton" type="button">' + 'Click to restyle the page so you can Nice Print! (It shouldn\'t alter your ACTUAL timetable\, but use at own risk)</button>';
ButtonNode.setAttribute('id', 'buttonContainer');
document.body.appendChild(ButtonNode);

// Monitor for clicks of the button
document.getElementById("nicePrintButton").addEventListener("click", ButtonClickAction, false);

console.log("Nice Print has created the button and is awaiting click");

///// The function that perform the page adjustments when the button is clicked
function ButtonClickAction() {
  console.log("Nice Print button has been clicked");
  ///// First we remove the unnecessary items on the page (identified by the IDs in the array)
  //Remove those identifiable by ID
  var ElementsToDeleteIdArray = ["ctl00_divUMMasterHeaderDisplay","ctl00_divUMNavBarHide","ctl00_h1PageTitle","ctl00_ctlMessageDisplay_InnerMsgListerInfoContainer","ctl00_divUMMasterSection2","ctl00_Content_divFilter","ctl00_Content_ctlNav","ctl00_Content_divActionTop","ctl00_Content_ctlActionBarBottom","ctl00_divUMHintBox","ctl00_Content_ctlTimetableMain_FlexiExt","ui-datepicker-div"];
  var ETDILength = ElementsToDeleteIdArray.length;
  //Loop over each element, removing them
  for (var i = 0; i < ETDILength; i++) {
    console.log(ElementsToDeleteIdArray[i]); //NOTE: if you misspell the argument then the rest of the code fails......
    var elementToDelete = document.getElementById(ElementsToDeleteIdArray[i]); //Targets the ___ to delete
    elementToDelete.remove();
  }
  //Remove this that is identifiable by class
  var elementToDelete1 = document.getElementsByClassName('page-header'); //Targets the ___ to delete
  console.log(elementToDelete1[0]);
  elementToDelete1[0].remove();
  //Adjust the left-margin to zero, that was left behind from deleting the "Legend" element
  var elementToStyleEdit1 = document.getElementById("ctl00_Content_ctlTimetableMain_ContainerOffset"); //NOTE: remember the "document."!
  console.log(elementToStyleEdit1);
  elementToStyleEdit1.setAttribute("style", "margin-left: 0px;");
  
  ///// Now we tweak the timetable
  //Set the dimensions
  var ColWidth = 137; 
  var TableHeight = 888;
  //Adjust the width of the section of the timetable that contains the class/lecture/tutorial/etc. blocks
  var TableElementBlocks = document.getElementById("ctl00_Content_ctlTimetableMain_DayGrp");
  TableElementBlocks.style.width = 5*ColWidth+'px';
  //Create an array of the day columns' IDs to loop over
  var WeekdayColumnIdArray = ["ctl00_Content_ctlTimetableMain_MonDayCol","ctl00_Content_ctlTimetableMain_TueDayCol","ctl00_Content_ctlTimetableMain_WedDayCol","ctl00_Content_ctlTimetableMain_ThuDayCol","ctl00_Content_ctlTimetableMain_FriDayCol"];
  var WCILength = WeekdayColumnIdArray.length;
  //Loop over each element and sets the widths
  for (var i = 0; i < WCILength; i++) {
    console.log(WeekdayColumnIdArray[i]); //NOTE: if you misspell the arg then the rest of the code fails......
    var elementToAdjust = document.getElementById(WeekdayColumnIdArray[i]); //Targets the ___ to adjust
    elementToAdjust.style.width = ColWidth+'px';
  }
  //Adjusts the height of the section of the timetable that contains the class/lecture/tutorial/etc. blocks 
  TableElementBlocks.style.height = TableHeight+'px';
  //Adjusts the height of the secion of the timetable that contains the times (the left-hand column)
  var TableElementTimes = document.getElementById("ctl00_Content_ctlTimetableMain_TimelineContainer");
  TableElementTimes.style.height = TableHeight+'px';
  console.log("Now next is the class block heights...");
  //Loop over each class block and adjust the height to match the time slot
  WeekdayColumnBodyPartIdArray = ["ctl00_Content_ctlTimetableMain_MonDayCol_Body_","ctl00_Content_ctlTimetableMain_TueDayCol_Body_","ctl00_Content_ctlTimetableMain_WedDayCol_Body_","ctl00_Content_ctlTimetableMain_ThuDayCol_Body_","ctl00_Content_ctlTimetableMain_FriDayCol_Body_"];
  for (var i = 0; i < 5; i++) {
    t=0;
    console.log(WeekdayColumnBodyPartIdArray[i]+t);
    while (document.getElementById(WeekdayColumnBodyPartIdArray[i]+t)!=null) {
      console.log(WeekdayColumnBodyPartIdArray[i]+t);
      console.log(document.getElementById(WeekdayColumnBodyPartIdArray[i]+t)!=null);
      ////identify the time span, store the number of hours (er.. or minutes) in a variable, then use that variable to adjust the height of the block
      //Locate the block
      BlockElement = document.getElementById(WeekdayColumnBodyPartIdArray[i]+t); console.log(BlockElement!=null);
      //Acquire the start and end times in inner elements
      console.log(WeekdayColumnBodyPartIdArray[i]+t+"_HiddenStartTm");
      console.log(document.getElementById(WeekdayColumnBodyPartIdArray[i]+t+"_HiddenStartTm")!=null);
      StartTime = document.getElementById(WeekdayColumnBodyPartIdArray[i]+t+"_HiddenStartTm").value;//element.getElementById doesn't work? use document. ?
      EndTime   = document.getElementById(WeekdayColumnBodyPartIdArray[i]+t+"_HiddenEndTm").value;console.log(StartTime+" "+EndTime);
      //check that the time strings are appropriately short, and convert it to minutes from midnight
      if (StartTime.length<8) {//e.g. as it's supposed to be: 12:00pm
        StartMinutesFromMidnight = ((parseInt(StartTime.split(":")[0],10)%12+12*(StartTime.split(":")[1][2]=="p"))*60+parseInt(StartTime.split(":")[1].substring(0,2),10));
        console.log(StartMinutesFromMidnight);
      } else {console.log("The start time string is too long!");};
      if (EndTime.length<8) {//e.g. as it's supposed to be: 12:00pm
        EndMinutesFromMidnight = ((parseInt(EndTime.split(":")[0],10)%12+12*(EndTime.split(":")[1][2]=="p"))*60+parseInt(EndTime.split(":")[1].substring(0,2),10));
        console.log(EndMinutesFromMidnight);
      } else {console.log("The end time string is too long!");};
      //Calculate the time span that the block occurs
      TimeLengthMinutes = EndMinutesFromMidnight - StartMinutesFromMidnight; console.log(TimeLengthMinutes);
      //Adjust the block height, noting that there is a total 11hr span, so 11*60 minute span
      BlockHeight = (TimeLengthMinutes/(11*60))*TableHeight; console.log(BlockHeight);
      BlockElement.style.height = BlockHeight+"px";console.log(BlockElement.style.height);
      t++; //See also: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Arithmetic_Operators#Increment_%28%29
    }
  }
  ////need to see what output wen get by id fails.  ctl00_Content_ctlTimetableMain_WedDayCol_Body_*** //need to identify any number of such classes
  // OK, so it returns null if nothing found. so need to end the loop for each day of the week when null
  
  
  
  //Delete the button after the buttonclick actions have been done
  document.getElementById("nicePrintButton").remove();
  console.log("Nice Print button event has finished");
}